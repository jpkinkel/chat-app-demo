namespace tests;

using Moq;
using System.Security.Cryptography.X509Certificates;
using ChatService.Controllers;
using Services.Chat.Models;

public class ChatControllerTests
{
    [Fact]
    public void GivenChatSvcThrowsError_ThenThrowsError()
    {
        // Arrange
        var chatCtr = setup(true);

        // Act
        Func<Task> act = () => chatCtr.Post(new Conversation());

        // Assert
        Assert.ThrowsAsync<Exception>(act);
    }

    [Fact]
    public async void GivenAnyNonEmptyConversation_ThenAnyStringResultIsReturned()
    {
        // Arrange
        var chatCtr = setup(false);

        // Act
        var result = await chatCtr.Post(
            new Conversation(
                new List<ChatMessage>() { new ChatMessage("test", ConversationRole.User) }
            )
        );

        // Assert
        Assert.True(!string.IsNullOrEmpty(result));
    }

    private ChatController setup(bool shouldThrowError)
    {
        var chatSvc = new Mock<Services.Chat.IChatService>();

        chatSvc
            .Setup(x => x.GetReply(It.IsAny<Conversation>()))
            .ReturnsAsync(
                (Conversation c) =>
                {
                    if (shouldThrowError)
                        throw new Exception();
                    return "result";
                }
            );
        var controller = new ChatController(chatSvc.Object);
        return controller;
    }
}