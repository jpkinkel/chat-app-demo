namespace tests;

using Moq;
using Services.Chat.Models;
using Services.Chat;

public class ChatServiceTests
{
    [Fact]
    public void GivenChatClientThrowsException_ThenExceptionThrown()
    {
        // Arrange
        var chatSvc = setup(true);

        // Act
        Func<Task> act = () => chatSvc.GetReply(new Conversation());

        // Assert
        Assert.ThrowsAsync<Exception>(act);
    }

    [Fact]
    public async void GivenAnyNonEmptyConversation_ThenAnyStringResultIsReturned()
    {
        // Arrange
        var chatSvc = setup(false);

        // Act
        var result = await chatSvc.GetReply(
            new Conversation(
                new List<ChatMessage>() { new ChatMessage("test", ConversationRole.User) }
            )
        );

        // Assert
        Assert.True(!string.IsNullOrEmpty(result));
    }

    private IChatService setup(bool shouldThrowError)
    {
        var chatClient = new Mock<Services.Chat.ChatClient.IChatClient>();
        chatClient
            .Setup(x => x.GetReply(It.IsAny<Conversation>()))
            .ReturnsAsync(
                (Conversation c) =>
                {
                    if (shouldThrowError)
                        throw new Exception();
                     
                    return "result";
                }
            );

        return new ChatService(chatClient.Object);
    }
}
