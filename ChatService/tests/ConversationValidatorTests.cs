namespace tests;

using Services.Chat.Models;

public class ConversationValidatorTests
{
    [Fact]
    public void GivenEmptyConversation_ThenOneErrorReturned()
    {
        // Arrange
        var validator = new ConversationValidator();
        var conversation = new Conversation();

        // Act
        var result = validator.validate(conversation);

        // Assert
        Assert.True(result.Count() == 1);
    }

    [Fact]
    public void GivenLastMessageEmpty_ThenOneErrorReturned()
    {
        // Arrange
        var validator = new ConversationValidator();
        var conversation = new Conversation()
        {
            Messages = new List<ChatMessage>() { new ChatMessage("", ConversationRole.User) }
        };

        // Act
        var result = validator.validate(conversation);
        Assert.True(result.Count() == 1);
    }

    [Fact]
    public void GivenLastMessageNoneUser_ThenOneErrorReturned()
    {
        // Arrange
        var validator = new ConversationValidator();
        var conversation = new Conversation()
        {
            Messages = new List<ChatMessage>() { new ChatMessage("test", ConversationRole.System) }
        };

        // Act
        var result = validator.validate(conversation);
        Assert.True(result.Count() == 1);
    }

    [Fact]
    public void GivenNormalConversation_ThenReturnsNonEmptyString()
    {
        // Arrange
        var validator = new ConversationValidator();
        var conversation = new Conversation()
        {
            Messages = new List<ChatMessage>() { new ChatMessage("test", ConversationRole.User) }
        };

        // Act
        var result = validator.validate(conversation);
        Assert.True(result.Count() == 0);
    }
}
