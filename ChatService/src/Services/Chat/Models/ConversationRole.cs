namespace Services.Chat.Models;

public enum ConversationRole
{
    System = 1,
    Assistant = 2,
    User = 3,
    Function = 4
}
