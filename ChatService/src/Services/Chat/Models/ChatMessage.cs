namespace Services.Chat.Models; 

public record struct ChatMessage (
    string Message,
    ConversationRole Role
);