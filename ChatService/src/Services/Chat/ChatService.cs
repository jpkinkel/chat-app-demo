namespace Services.Chat;

using Services.Chat.ChatClient;
using Services.Chat.Models;

public class ChatService : IChatService
{
    private readonly IChatClient _chatClient;

    public ChatService(IChatClient chatClient)
    {
        _chatClient = chatClient;
    }

    public async Task<string> GetReply(Conversation conversation)
    {
        return await _chatClient.GetReply(conversation);
    }
}
