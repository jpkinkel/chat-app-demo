namespace Services.Chat.ChatClient;

using Services.Chat.Models;

public interface IChatClient
{
    /// <summary>
    /// Get a reply from the chat service
    /// </summary>
    /// <param name="conversation">The previous conversation plus the latest message added by the user</param>
    /// <returns>The reply from the chat service</returns>
    Task<string> GetReply(Conversation conversation) ;
}