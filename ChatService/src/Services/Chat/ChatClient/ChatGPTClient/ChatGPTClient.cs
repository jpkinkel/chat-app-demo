namespace Services.Chat.ChatClient.ChatGPTClient;

using OpenAI;
using OpenAI.Chat;
using OpenAI.Models;
using Services.Chat.ChatClient;
using Services.Chat.Models;

public class ChatGPTClient : IChatClient
{
    private OpenAIClient _openAIClient;

    public ChatGPTClient(string apiKey)
    {
        _openAIClient = new OpenAIClient(apiKey);
    }

    public async Task<string> GetReply(Conversation conversation)
    {
        // validate conversation
        var validationErrors = new ConversationValidator().validate(conversation);
        if (validationErrors.ToList().Count > 0)
        {
            throw new Exception(validationErrors.ToString());
        }
        
        // convert conversation to messages to ChatRequest
        var messages = new List<Message>();
        foreach (var message in conversation.Messages)
        {
            messages.Add(new Message(map(message.Role), message.Message));
        }
        var chatRequest = new ChatRequest(messages, Model.GPT3_5_Turbo_16K);

        // get reply from ChatGPT
        var result = await _openAIClient.ChatEndpoint.GetCompletionAsync(chatRequest);

        // log it
        Console.WriteLine(
            $"{result.FirstChoice.Message.Role}: {result.FirstChoice.Message.Content}"
        );

        //return string result only
        return result.FirstChoice.Message.Content;
    }

    private Role map(ConversationRole role)
    {
        return role switch
        {
            ConversationRole.User => Role.User,
            ConversationRole.System => Role.System,
            ConversationRole.Assistant => Role.Assistant,
            ConversationRole.Function => Role.Function,
            _ => throw new NotImplementedException()
        };
    }
}
