using Services.Chat.Models;

public class ConversationValidator
{
    /**
    * Validate a conversation - to do - replace string literals with constants
    * @param conversation The conversation to validate
    * @return A list of errors
    */
    public IEnumerable<string> validate(Conversation conversation)
    {
        var errors = new List<string>();
        if (conversation.Messages == null || conversation.Messages.Count() == 0)
        {
            errors.Add("Conversation must have at least one message");
            return errors;
        }
        if (conversation.Messages.Last().Role != ConversationRole.User)
            errors.Add("Conversation must end with user role");

        if (string.IsNullOrEmpty(conversation.Messages.Last().Message))
            errors.Add("Conversation must end with non-empty message");

        return errors;
    }
}
