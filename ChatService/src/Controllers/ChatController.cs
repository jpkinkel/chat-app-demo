using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Services.Chat.Models;

namespace ChatService.Controllers;

[ApiController]
[Route("[controller]")]
[Authorize("RequireInteractiveUser")]
public class ChatController : ControllerBase
{
    private Services.Chat.IChatService _chatService;
    private const string validationErrorMessage = "Conversation must have at least one message.";

    public ChatController(Services.Chat.IChatService chatService)
    {
        _chatService = chatService;
    }

    [HttpPost(Name = "GetReply")]
    public async Task<string> Post([FromBody] Conversation conversation)
    {
        return await _chatService.GetReply(conversation);
    }
}
