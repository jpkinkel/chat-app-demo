using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Services.Chat.ChatClient;
using Services.Chat.ChatClient.ChatGPTClient;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();

builder.Services
    .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.Authority = "https://localhost:5001";
        options.TokenValidationParameters.ValidateAudience = false;
        options.TokenValidationParameters.ValidateLifetime = true;
        options.TokenValidationParameters.ClockSkew = TimeSpan.Zero;
        options.MapInboundClaims = false;
        
    });

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy(
        "ApiCaller",
        policy =>
        {
            policy.RequireClaim("scope", "api");
        }
    );

    options.AddPolicy(
        "RequireInteractiveUser",
        policy =>
        {
            policy.RequireClaim("sub");
        }
    );
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var chatGPTClient = new ChatGPTClient(builder.Configuration.GetValue<string>("ChatGPTApiKey"));
builder.Services.AddSingleton<IChatClient>(provider => chatGPTClient);
builder.Services.AddSingleton<Services.Chat.IChatService,Services.Chat.ChatService>();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
