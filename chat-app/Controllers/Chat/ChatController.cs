namespace chat_app.Controllers.Chat;

using chat_app.Controllers.Chat.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Chat.Models;
using Services.Chat;

public class ChatController : Controller
{
    private readonly IChatService _chatService;

    public ChatController(IChatService chatService)
    {
        _chatService = chatService;
    }

    [HttpPost("getReply")]
    [Authorize]
    public async Task<IActionResult> GetReply([FromBody] Conversation conversation)
    {
        try {
            var result = await _chatService.GetReply(conversation);
            return Ok(new Reply(Message: result ));
        } catch(UnauthorizedAccessException ex) {
            return Unauthorized(ex.Message);
        } catch(Exception ex) {
            return BadRequest(ex.Message);
        }
    }
}