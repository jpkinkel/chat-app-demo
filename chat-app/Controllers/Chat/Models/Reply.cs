namespace chat_app.Controllers.Chat.Models;
public record struct Reply (
    string Message
);