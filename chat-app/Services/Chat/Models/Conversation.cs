namespace Services.Chat.Models;
public record struct Conversation(IEnumerable<ChatMessage> Messages);