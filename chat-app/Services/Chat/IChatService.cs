using Services.Chat.Models;

namespace Services.Chat;

public interface IChatService
{
    Task<string> GetReply(Conversation conversation);
}
