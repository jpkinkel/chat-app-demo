namespace Services.Chat;

using System.Text;
using System.Text.Json;
using Services.Chat.Models;
public class ChatService : IChatService
{

        private readonly HttpClient _httpClient;
    
    public ChatService(IHttpClientFactory httpClientFactory)
    {
        _httpClient = httpClientFactory.CreateClient("chatsvc");
    }

    public async Task<string> GetReply(Conversation conversation)
    {
        // Make the request to the external API - 
        //todo :  URL should be in config
        HttpResponseMessage response = await _httpClient.PostAsync(
            "http://localhost:5093/chat", 
            new StringContent(JsonSerializer.Serialize(conversation), Encoding.UTF8, "application/json")
        );

        // Process the response from the external API and return the data
        if (response.IsSuccessStatusCode)
        {
         return  await response.Content.ReadAsStringAsync();
        }
        else if(response.StatusCode == System.Net.HttpStatusCode.Unauthorized) {
            throw new UnauthorizedAccessException(response.ReasonPhrase);
        }
        else
        {
            throw new Exception(response.ReasonPhrase);
        }
    }
}
