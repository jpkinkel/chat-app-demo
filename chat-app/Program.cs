using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text.Json;
using Duende.Bff.Yarp;
using Microsoft.AspNetCore.Authorization;
using Services.Chat;

var builder = WebApplication.CreateBuilder(args);

JwtSecurityTokenHandler.DefaultMapInboundClaims = false;
builder.Services.AddAuthorization();

builder.Services
.AddBff()
.AddRemoteApis();

builder.Services
.AddControllers()
.AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
    });

builder.Services
.AddAuthentication(options =>
{
    options.DefaultScheme = "Cookies";
    options.DefaultChallengeScheme = "oidc";
    options.DefaultSignOutScheme = "oidc";
})
.AddCookie("Cookies")
.AddOpenIdConnect("oidc", options =>
{
    options.Authority = "https://localhost:5001";
    options.ClientId = "bff";
    options.ClientSecret = "secret";
    options.ResponseType = "code";
    options.Scope.Add("chat_assistant");
    options.Scope.Add("offline_access");
    options.SaveTokens = true;
    options.GetClaimsFromUserInfoEndpoint = true;
});


builder.Services.AddHttpContextAccessor();
builder.Services.AddScoped<IChatService,ChatService>();
builder.Services.AddHttpClient("chatsvc").AddUserAccessTokenHandler();

var app = builder.Build();

app.UseStaticFiles();
app.UseRouting();
app.UseAuthentication();

app.UseBff();
app.UseAuthorization();
app.MapBffManagementEndpoints();
app.MapControllers()
    .RequireAuthorization()
    .AsBffApiEndpoint();

app.Run();

[Authorize] 
static IResult LocalIdentityHandler(ClaimsPrincipal user, HttpContext context)
{
    var name = user.FindFirst("name")?.Value ?? user.FindFirst("sub")?.Value;
    return Results.Json(new { message = "Local API Success!", user = name });
}