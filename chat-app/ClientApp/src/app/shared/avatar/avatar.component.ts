import { Component, Input } from "@angular/core";

@Component({
    selector: "app-avatar",
    templateUrl: "./avatar.component.html"
})
export class AvatarComponent {
    @Input() 
    iconClass = "bi bi-person-circle" 

    constructor() {}
}
