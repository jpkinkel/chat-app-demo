import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AvatarComponent } from "./avatar/avatar.component";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";

@NgModule({
    declarations: [
        AvatarComponent
    ],
    imports: [
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [
        ReactiveFormsModule,
        AvatarComponent
    ],
    providers: [],
    bootstrap: []
})
export class SharedModule { }
