import { Component, OnInit } from "@angular/core";
import { AuthService } from "../services/auth/auth.service";

@Component({
    selector: "app-silent-login",
    templateUrl: "./silent-login.component.html"
})
export class SilentLoginComponent {
    isLoggedIn = this.authService.observeAuthState()

    constructor(
        private readonly authService: AuthService
    ) {
        window.addEventListener("message", e => {
            if (e.data && e.data.source === 'bff-silent-login' && e.data.isLoggedIn) {
                console.log("silent login successfull", e.data)
                authService.revalidateSession()
            }
        });
    }
}