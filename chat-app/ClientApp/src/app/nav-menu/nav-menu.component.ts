import { Component, Inject, OnDestroy } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';
import { Subject, filter, takeUntil } from 'rxjs';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnDestroy {
  isExpanded = false;
  isLoggedIn = this.authService.observeAuthState()
  unsubscribeSubject = new Subject<void>()
  username : string | null | undefined = null

  constructor(
    @Inject('BASE_URL') private baseUrl: string,
    private readonly authService: AuthService,
  ) {
    this
      .isLoggedIn
      .pipe(
        takeUntil(this.unsubscribeSubject),
        filter(isLoggedIn => isLoggedIn !== null)
      )
      .subscribe(_ => {
        this.username = authService.getAuthenticatedUser()?.fullName
      })
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject.next()
    this.unsubscribeSubject.complete()
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  goToLoginPage() {
    window.location.href = this.baseUrl + "bff/login"
  }
  async logout() {
    let userClaims
    var req = new Request(this.baseUrl + "bff/user", {
      headers: new Headers({
        "X-CSRF": "1",
      }),
    });

    try {
      var resp = await fetch(req);
      if (resp.ok) {
        userClaims = await resp.json();

        console.log("user logged in", userClaims);
      } else if (resp.status === 401) {
        console.log("user not logged in");
      }
    } catch (e) {
      console.log("error checking user status");
    }

    if (userClaims) {
      var logoutUrl = userClaims.find(
        (claim: any) => claim.type === "bff:logout_url"
      ).value;
      window.location.href = logoutUrl;
    } else {
      window.location.href = "/bff/logout";
    }
  }
}
