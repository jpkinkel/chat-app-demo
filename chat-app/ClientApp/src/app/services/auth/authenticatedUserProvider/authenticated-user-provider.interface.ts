import { AuthenticatedUser } from "../models/authenticated-user";

export interface AuthenticatedUserProviderInterface {
    getAuthenticatedUser() : AuthenticatedUser | null
    setAuthenticatedUser(authenticatedUser : AuthenticatedUser | null) : void
}