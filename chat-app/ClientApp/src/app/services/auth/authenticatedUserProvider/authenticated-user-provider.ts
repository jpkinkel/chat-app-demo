import { Injectable } from "@angular/core";
import { AuthenticatedUserProviderInterface } from "./authenticated-user-provider.interface";
import { AuthenticatedUser } from "../models/authenticated-user";

@Injectable({
    providedIn: 'root'
})
export class AuthenticatedUserProvider implements AuthenticatedUserProviderInterface {
    private authenticatedUser: AuthenticatedUser | null = null

    getAuthenticatedUser(): AuthenticatedUser | null {
        return this.authenticatedUser
    }

    setAuthenticatedUser(authenticatedUser: AuthenticatedUser | null): void {
        this.authenticatedUser = authenticatedUser
    }
}