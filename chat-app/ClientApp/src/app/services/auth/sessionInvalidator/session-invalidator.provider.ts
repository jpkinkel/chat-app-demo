import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";

@Injectable({ providedIn: 'root' })
export class SessionInvalidatorProvider {
    subject = new Subject<void>();
    
    constructor() {}
    
    observeInvalidation(): Observable<void> {
        return this.subject.asObservable();
    }

    invalidate() {
        this.subject.next();
    }
}