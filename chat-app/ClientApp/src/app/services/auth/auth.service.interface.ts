import { Observable } from "rxjs";
import { AuthenticatedUser } from "./models/authenticated-user";

export interface AuthServiceInterface {
    observeAuthState(): Observable<boolean | null>;
    getAuthenticatedUser() : AuthenticatedUser | null;
    invalidateSession() : void;
    revalidateSession() : void;    
}