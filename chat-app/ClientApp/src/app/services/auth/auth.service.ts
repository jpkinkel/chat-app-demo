import { BehaviorSubject, Observable, firstValueFrom } from "rxjs";
import { AuthServiceInterface } from "./auth.service.interface";
import { AuthApiClient } from "../api/auth/auth.apiclient";
import { Injectable } from "@angular/core";
import { AuthenticatedUserProvider } from "./authenticatedUserProvider/authenticated-user-provider";
import { AuthenticatedUser } from "./models/authenticated-user";
import { SessionInvalidatorProvider } from "./sessionInvalidator/session-invalidator.provider";

@Injectable({
    providedIn: 'root'
})
export class AuthService implements AuthServiceInterface {
    private authStateSubject = new BehaviorSubject<boolean | null>(null);

    constructor(
        private readonly authApiClient: AuthApiClient,
        private readonly sessionInvalidator: SessionInvalidatorProvider,
        private readonly authenticatedUserProvider: AuthenticatedUserProvider
    ) {
        this.determineAuthState().then()
        this.observeSessionInvalidation()
    }

    private observeSessionInvalidation(): void {
        this.sessionInvalidator.observeInvalidation().subscribe(() => {
            this.invalidateSession()
        })
    }

    observeAuthState(): Observable<boolean | null> {
        return this.authStateSubject.asObservable()
    }

    getAuthenticatedUser(): AuthenticatedUser | null {
        return this.authenticatedUserProvider.getAuthenticatedUser()
    }

    invalidateSession(): void {
        this.authStateSubject.next(false)
        this.authenticatedUserProvider.setAuthenticatedUser(null)
    }

    revalidateSession() : void {
        this.determineAuthState().then()
    }

    private async determineAuthState(): Promise<void> {
        try {
            const userClaims = await firstValueFrom(this.authApiClient.getUserClaims())
            console.log("userclaims", userClaims)
            if (userClaims) {
                this.authenticatedUserProvider.setAuthenticatedUser({
                    fullName: userClaims.find(claim => claim.type === 'name')?.value,
                })
                console.log("user is logged in!")
                this.authStateSubject.next(true)
                return
            }
        } catch (error) {
            console.log("user is NOT logged in! error: ", error)

        }
        this.authStateSubject.next(false)
    }
}