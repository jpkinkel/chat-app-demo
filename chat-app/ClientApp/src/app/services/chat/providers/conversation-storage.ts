import { Injectable } from "@angular/core";
import { Conversation } from "../models/conversation";
import { ConversationStorageInterface } from "./conversation-storage.interface";
import { StorageProvider } from "../../storage/storage";

@Injectable({
    providedIn: 'root'
})
export class ConversationStorage implements ConversationStorageInterface {    
    readonly storageKey = "conversation"

    constructor(
        private readonly storageProvider : StorageProvider
    ) {}

    storeConversation(conversation: Conversation): void {
        this.storageProvider.set(this.storageKey, JSON.stringify(conversation))
    }

    retrieveConversation(): Conversation {
        const result = this.storageProvider.get(this.storageKey) 
        if (result) {
            return JSON.parse(result)
        }
        return { messages: [] } as Conversation
    }
}