import { Conversation } from "../models/conversation"

export interface ConversationStorageInterface {
    storeConversation(conversation: Conversation ): void
    retrieveConversation(): Conversation 
}