import { TestBed } from '@angular/core/testing';
import { StorageProvider } from '../../storage/storage';
import { ConversationStorage } from './conversation-storage';
import { ConversationRole } from '../models/conversation-role';

describe('ConversationStorage', () => {
  let service: ConversationStorage;
  let mockStorageProvider: jasmine.SpyObj<StorageProvider>;

  beforeEach(() => {
    mockStorageProvider = jasmine.createSpyObj('StorageProvider', ['set', 'get', 'remove', 'clear']);

    TestBed.configureTestingModule({
      providers: [
        ConversationStorage,
        { provide: StorageProvider, useValue: mockStorageProvider }
      ]
    });

    service = TestBed.inject(ConversationStorage);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should store a conversation correctly', () => {
    const testConversation = { messages: [{ message: 'Hello', role: ConversationRole.user }] };

    service.storeConversation(testConversation);

    expect(mockStorageProvider.set).toHaveBeenCalledWith(service.storageKey, JSON.stringify(testConversation));
  });

  it('should retrieve a conversation correctly', () => {
    const testConversation = { messages: [{ message: 'Hello', role: ConversationRole.user }] };
    mockStorageProvider.get.and.returnValue(JSON.stringify(testConversation));

    const result = service.retrieveConversation();

    expect(result).toEqual(testConversation);
    expect(mockStorageProvider.get).toHaveBeenCalledWith(service.storageKey);
  });

  it('should return an empty conversation when none is in storage', () => {
    mockStorageProvider.get.and.returnValue(null);

    const result = service.retrieveConversation();

    expect(result).toEqual({ messages: [] });
    expect(mockStorageProvider.get).toHaveBeenCalledWith(service.storageKey);
  });
});