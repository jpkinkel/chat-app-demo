import { TestBed } from '@angular/core/testing';
import { ChatApiClient } from '../api/chat/chat.apiclient';
import { ConversationStorage } from './providers/conversation-storage';
import { of, throwError } from 'rxjs';
import { ChatService } from './chat.service';

describe('ChatService', () => {
  let service: ChatService;
  let mockChatApiClient: jasmine.SpyObj<ChatApiClient>;
  let mockConversationStorage: jasmine.SpyObj<ConversationStorage>;

  beforeEach(() => {
    mockChatApiClient = jasmine.createSpyObj('ChatApiClient', ['getReply']);
    mockConversationStorage = jasmine.createSpyObj('ConversationStorage', ['storeConversation', 'retrieveConversation']);
    mockConversationStorage.retrieveConversation.and.returnValue({ messages: [] });
    TestBed.configureTestingModule({
      providers: [
        ChatService,
        { provide: ChatApiClient, useValue: mockChatApiClient },
        { provide: ConversationStorage, useValue: mockConversationStorage }
      ]
    });

    service = TestBed.inject(ChatService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add user message and api reply to the conversation', async () => {
    const testMessage = 'Hello!';
    const testReply = 'Hi!';

    mockChatApiClient.getReply.and.returnValue(of(testReply));

    await service.addToConversation(testMessage);

    expect(mockConversationStorage.storeConversation).toHaveBeenCalledTimes(1);
    expect(mockChatApiClient.getReply).toHaveBeenCalled();
  });

  it('should handle api errors gracefully', async () => {
    const testMessage = 'Hello!';

    mockChatApiClient.getReply.and.returnValue(throwError(() => {
      return { status: 401, message: 'Unauthorized' }
    }));

    await service.addToConversation(testMessage);

    expect(mockConversationStorage.storeConversation).toHaveBeenCalledTimes(1);
    expect(mockChatApiClient.getReply).toHaveBeenCalled();
  });

  it('should allow observation of conversation changes', (done) => {
    service.observeConversation().subscribe(conversation => {
      expect(conversation).toBeTruthy();
      done();
    });
  });
});
