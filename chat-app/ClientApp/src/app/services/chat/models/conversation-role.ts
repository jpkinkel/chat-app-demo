export enum ConversationRole
{
    system = 1,
    assistant = 2,
    user = 3,
    function = 4
}
