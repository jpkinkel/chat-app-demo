import { ChatMessage } from "./chat-message";

export interface Conversation {
    messages: ChatMessage[]
}