import { Conversation } from "./conversation"
import { ConversationRole } from "./conversation-role"

export interface ChatMessage {
  message: string
  role: ConversationRole
}