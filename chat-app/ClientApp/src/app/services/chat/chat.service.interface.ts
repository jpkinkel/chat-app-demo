import { Observable } from "rxjs";
import { Conversation } from "./models/conversation";

export interface ChatServiceInterface {
    addToConversation(chatMessage: string): Promise<void>
    observeConversation(): Observable<Conversation>
}