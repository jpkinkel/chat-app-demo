import { Injectable } from "@angular/core";
import { ChatServiceInterface } from "./chat.service.interface";

import { BehaviorSubject, Observable, firstValueFrom } from "rxjs";
import { ChatApiClient } from "../api/chat/chat.apiclient";
import { ConversationStorage } from "./providers/conversation-storage";
import { ConversationRole } from "./models/conversation-role";
import { HttpErrorResponse } from "@angular/common/http";
import { Conversation } from "./models/conversation";

@Injectable({
    providedIn: 'root'
})
export class ChatService implements ChatServiceInterface {
    private conversationSubject = new BehaviorSubject<Conversation>(this.conversationStorage.retrieveConversation())

    constructor(
        private readonly chatApiClient: ChatApiClient,
        private readonly conversationStorage: ConversationStorage,
    ) { }

    /**
     * Add a message to the conversation. The function does not throw an error. 
     * When error occurs the error is added to the conversation.
     * @param chatMessage   The message to add to the conversation
     * 
     */
    async addToConversation(chatMessage: string): Promise<void> {
        // get current conversation
        let conversation = this.conversationSubject.getValue()
        if (!conversation) {
            conversation = { messages: [] } as Conversation
        }

        // add user message to the conversation 
        conversation.messages.push({
            message: chatMessage,
            role: ConversationRole.user,
        })

        //publish conversation
        this.conversationSubject.next(conversation)

        // get reply from the api
        let reply = ""
        let role = ConversationRole.assistant
        try {
            reply = await firstValueFrom(this.chatApiClient.getReply(conversation))
        } catch (error) {
            role = ConversationRole.system
            if (error instanceof HttpErrorResponse) {
                switch ((error as HttpErrorResponse).status) {
                    case 401:
                        reply = "You are not authorized to use this service, please log in"
                        break;
                    default:
                        reply = (error as HttpErrorResponse).message
                        break;
                }
            }
        }

        // add reply to the conversation 
        conversation.messages.push({
            message: reply,
            role: role,
        })

        // store the conversation 
        this.conversationStorage.storeConversation(conversation)

        //publish conversation
        this.conversationSubject.next(conversation)
    }

    observeConversation(): Observable<Conversation> {
        return this.conversationSubject.asObservable()
    }
}