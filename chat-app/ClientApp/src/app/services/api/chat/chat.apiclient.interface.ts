import { Observable } from "rxjs";
import { Conversation } from "../../chat/models/conversation";

export interface ChatApiClientInterface {
    getReply(conversation: Conversation): Observable<string>
}