import { Observable, map } from "rxjs";
import { Conversation } from "../../chat/models/conversation";
import { ChatApiClientInterface } from "./chat.apiclient.interface";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class ChatApiClient implements ChatApiClientInterface {
    constructor(
        private readonly httpClient: HttpClient,
    ) { }

    getReply(conversation: Conversation): Observable<string> {
        return this.httpClient.post(`getReply`,
            conversation, {
            headers: {
                "X-CSRF": "1",
            },
        }).pipe(map(response => {
            return (response as any).message as string
        }))
    }
}