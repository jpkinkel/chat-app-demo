import { Observable } from "rxjs";

export interface AuthApiClientInterface {
    getUserClaims() : Observable<any[]>
}