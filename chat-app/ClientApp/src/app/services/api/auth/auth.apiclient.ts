import { Inject, Injectable } from "@angular/core";
import { AuthApiClientInterface } from "./auth.apiclient.interface";
import { HttpClient } from "@angular/common/http";
import { Observable, of, timer } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class AuthApiClient implements AuthApiClientInterface {
    constructor(
        private readonly httpClient: HttpClient,
        @Inject('BASE_URL') private baseUrl: string
    ) {}

    getUserClaims(): Observable<any[]> {
        return this.httpClient.get<any[]>(`${this.baseUrl}bff/user`, {
            headers: {
                "X-CSRF": "1",
            },
        })
    }
}