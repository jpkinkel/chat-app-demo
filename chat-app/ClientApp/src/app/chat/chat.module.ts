import { NgModule } from "@angular/core";
import { ChatConversationComponent } from "./chat-conversation/chat-conversation.component";
import { SharedModule } from "../shared/shared.module";
import { CommonModule } from "@angular/common";

@NgModule({
    declarations: [
        ChatConversationComponent
    ],
    imports: [
        CommonModule,
        SharedModule
    ],
    exports: [
        ChatConversationComponent
    ],
    providers: [],
    bootstrap: []
})
export class ChatModule { }
