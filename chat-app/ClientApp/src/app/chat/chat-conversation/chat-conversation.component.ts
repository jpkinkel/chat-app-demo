import { HttpErrorResponse } from "@angular/common/http";
import { Component, Inject, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Observable } from "rxjs";
import { ChatService } from "src/app/services/chat/chat.service";
import { Conversation } from "src/app/services/chat/models/conversation";
import { ConversationRole } from "src/app/services/chat/models/conversation-role";

@Component({
    selector: "chat-conversation",
    templateUrl: "./chat-conversation.component.html"
})
export class ChatConversationComponent implements OnInit {
    conversation: Observable<Conversation> = this.chatService.observeConversation()
    roleObject = ConversationRole

    form: FormGroup = new FormGroup({
        text: new FormControl("", Validators.required),
    });;

    constructor(
        private readonly chatService: ChatService
    ) { }

    ngOnInit(): void {
    }

    async addToConversation() {
        if (!this.form.valid) {
            return
        }
        this.form.reset()
        await this.chatService.addToConversation(this.form.value.text)
    }

    determineAvatarIcon(role: ConversationRole): string {
        switch (role) {
            case ConversationRole.user:
                return "bi bi-person-circle"
            case ConversationRole.assistant:
                return "bi bi-robot"
            case ConversationRole.system:
                return "bi bi-bug"
        }
        return "bi bi-person-circle"
    }

    determineBorder(role: ConversationRole): string {
        switch (role) {
            case ConversationRole.assistant:
                return "border border-dark rounded"
            case ConversationRole.system:
                return "border border-danger rounded"
        }
        return ""
    }
}
