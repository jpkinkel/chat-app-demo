import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ChatService } from 'src/app/services/chat/chat.service';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { ChatConversationComponent } from './chat-conversation.component';
import { ConversationRole } from 'src/app/services/chat/models/conversation-role';
import { Component } from '@angular/core';

@Component({ selector: 'app-avatar', template: '' })
class AvatarStubComponent {
}

describe('ChatConversationComponent', () => {

    let component: ChatConversationComponent;
    let fixture: ComponentFixture<ChatConversationComponent>;
    let chatService: ChatService;

    beforeEach(async () => {
        const chatServiceMock = {
            observeConversation: jasmine.createSpy('observeConversation').and.returnValue(of({
                messages: [
                    { role: ConversationRole.user, message: 'test message' },
                    { role: ConversationRole.system, message: 'system message' }
                ]
            })),
            addToConversation: jasmine.createSpy('addToConversation').and.returnValue(Promise.resolve())
        };

        await TestBed.configureTestingModule({
            declarations: [ChatConversationComponent, AvatarStubComponent],
            imports: [ReactiveFormsModule],
            providers: [
                { provide: ChatService, useValue: chatServiceMock }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(ChatConversationComponent);
        component = fixture.componentInstance;
        chatService = TestBed.inject(ChatService);
        fixture.detectChanges();
    });
    it('should render initial chat message', () => {
        const initialMessage = fixture.debugElement.query(By.css('.p-2')).nativeElement;
        expect(initialMessage.textContent).toContain('Hi, how can I help you?');
    });

    it('should render messages from the conversation observable', () => {
        const messages = fixture.debugElement.queryAll(By.css('.p-2 p'));
        expect(messages.length).toBe(2);
        expect(messages[0].nativeElement.textContent).toBe('test message');
    });

    it('should call addToConversation() when sending a message', () => {
        component.form.controls['text'].setValue('Hello');
        const sendButton = fixture.debugElement.query(By.css('button')).nativeElement;
        sendButton.click();
        fixture.detectChanges();
        expect(chatService.addToConversation).toHaveBeenCalledWith('Hello');
    });

    it('should correctly render and style a message of type system', () => {
        // Query all message paragraphs
        const messages = fixture.debugElement.queryAll(By.css('.p-2 p'));

        // Find the system message based on our mock data (it's the second message)
        const systemMessage = messages[1].nativeElement;

        // Check the text content
        expect(systemMessage.textContent).toBe('system message');

        // Check if it has the `text-danger` class for styling

        const isStyledCorrectly = systemMessage.classList.contains('text-danger');
        expect(isStyledCorrectly).toBeTrue();
    });

    describe('determineBorder', () => {
        it('should return correct border for assistant', () => {
            const result = component.determineBorder(ConversationRole.assistant);
            expect(result).toBe('border border-dark rounded');
        });
        it('should return correct border for system', () => {
            const result = component.determineBorder(ConversationRole.system);
            expect(result).toBe('border border-danger rounded');
        });
        it('should return default border for unknown role', () => {
            const result = component.determineBorder('unknown' as any);
            expect(result).toBe('');
        });
    });

    describe('determineAvatarIcon', () => {
        it('should return correct icon for user', () => {
            const result = component.determineAvatarIcon(ConversationRole.user);
            expect(result).toBe('bi bi-person-circle');
        });
        it('should return correct icon for assistant', () => {
            const result = component.determineAvatarIcon(ConversationRole.assistant);
            expect(result).toBe('bi bi-robot');
        });
        it('should return correct icon for system', () => {
            const result = component.determineAvatarIcon(ConversationRole.system);
            expect(result).toBe('bi bi-bug');
        });
        it('should return default icon for unknown role', () => {
            const result = component.determineAvatarIcon('unknown' as any);
            expect(result).toBe('bi bi-person-circle');
        });
    });
});
