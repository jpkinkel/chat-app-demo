import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable, catchError, throwError } from "rxjs";
import { AuthService } from "../services/auth/auth.service";
import {  Injectable } from "@angular/core";
import { SessionInvalidatorProvider } from "../services/auth/sessionInvalidator/session-invalidator.provider";

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
    constructor(
        private readonly sessionInvalidator: SessionInvalidatorProvider,
    ) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req)
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    if (error.status === 401 || error.status === 403) {
                        this.sessionInvalidator.invalidate();
                    }
                    return throwError(error);
                })
            );
    }
}
