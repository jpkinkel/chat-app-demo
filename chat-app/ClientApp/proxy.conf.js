const { env } = require('process');

const target = 'https://localhost:7210';

const PROXY_CONFIG = [
  {
    context: [
      "/chat",
      "/getReply",
      "/send",
      "/bff",
      "/signin-oidc",
      "/signout-callback-oidc",

    ],
    proxyTimeout: 10000,
    target: target,
    secure: false,
    headers: {
      Connection: 'Keep-Alive'
    }
  }
]

module.exports = PROXY_CONFIG;
