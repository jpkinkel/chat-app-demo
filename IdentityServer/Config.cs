﻿using Duende.IdentityServer.Models;
using Duende.IdentityServer;

namespace IdentityServer;

public static class Config
{
    public static IEnumerable<IdentityResource> IdentityResources =>
        new IdentityResource[] { new IdentityResources.OpenId(), new IdentityResources.Profile(), };

    public static IEnumerable<ApiScope> ApiScopes =>
        new List<ApiScope> { new ApiScope(name: "chat_assistant", displayName: "Use Chat Assistant") };

    public static IEnumerable<Client> Clients =>
        new List<Client>
        {
            new Client
            {
                ClientId = "bff",
                AccessTokenLifetime = 15,	
                ClientSecrets = { new Secret("secret".Sha256()) },
                AllowedGrantTypes = GrantTypes.Code,
                AllowOfflineAccess = true,
                RefreshTokenUsage = TokenUsage.ReUse,
                RedirectUris = { "https://localhost:44495/signin-oidc" },
                PostLogoutRedirectUris = { "https://localhost:44495/signout-callback-oidc" },
                AllowedScopes = new List<string>
                {
                    IdentityServerConstants.StandardScopes.OpenId,
                    IdentityServerConstants.StandardScopes.Profile,
                    "chat_assistant"
                }
            }
        };
}
