# Background

  

The chat-app-demo was created to demonstrate a small scale micro services architecture using ASP.NET Core and Angular

# Features
 

- The app is basically a web based chat bot that takes the user 's input and gets a reply from a chat client implementation (only Chat GPT is implemented at this point)

- Authentication is handled by Duende Identity Server. Only Google Authentication is enabled. It supports an authorization code flow that is utilized by the web app.

- Interactive user authentication by using the BFF pattern. The Angular client uses a Backend for Frontend server that handles authentication.

  

- Chat Service is implemented as a separate micro service that can only be called by authenticated clients. In this case the client is the BFF server that takes the access token from the Angular client to call the service.

  

  

# ToDo and shortcomings

## General
 - All .NET services were implemented as separate projects with Visual Studio Code, so there are no solution files that do anything.
- This is not a production-ready repository.
- Functionally not quite finished
- URL configurations are not present.
- There is no CICD in place.

## Angular web app
- Todo - add button to clear conversation
- Todo - auto scroll to bottom of conversation

## Identity server
- Todo - check whether the token verification can be protected in such a way that it cannot be publicly called.
- Todo - add waiting indicator (Bot is typing...)

## BFF 
- Todo - reply should also include timestamp and meta data
- Todo - check server timeouts (a difficult question can result in server timeouts)
- Todo - possibly introduce a websocket to stream the conversation

  ## Chat Service
-  Todo - extend reply with timestamp and metadata etc. 

# Architecture


![enter image description here](https://lh3.googleusercontent.com/drive-viewer/AITFw-zzXvzuOuXY-i1JqsjVNQ7JekO2EQU94q038U_XKgXx6ht2rmx30YtC_YizZHlXyrrQNyhnR4qywfw-OSSDz7BGzXjNCQ=s2560)

  
### Chat App

- A simple single-page-application that mimicks a chat gpt conversation.

- It uses the BFF as an API gateway. The whole current conversation is sent to the server to get a reply from the Chat Service
  

### Identity
- Auth server that is basically a Duende Identity Server that supports one main authentication flow, authorization code.
### Chat Service
- Exposes one endpoint '/chat' that is called from the BFF server with a conversation object as the parameter.
- The chat endpoint returns a string that is the reply from the chat client (for now this is string-only, preferably should be a complex object)
### BFF
- The Backend for Frontend Implementation
- Handles authentication by exposing BFF Management endpoints and interacting with Duende Identity Server.
- Acts as an API gateway for the Angular Client.